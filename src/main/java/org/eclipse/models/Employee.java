package org.eclipse.models;

import java.util.Date;
import java.util.List;

public class Employee {

	private int id;
	private String firstName;
	private String lastName;
	private List<String> roles;
	private Date birthDate;
	

	public Employee(int id, String firstName, String lastName, List<String> roles, Date birthDate) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.roles = roles;
		this.birthDate = birthDate;
	}
	public Employee() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", roles=" + roles
				+ ", birthDate=" + birthDate + "]";
	}

	
	
	
	
}
