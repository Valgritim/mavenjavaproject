package org.eclipse.classes;

import java.util.Date;
import java.util.Arrays;

import org.eclipse.models.Employee;
import org.eclipse.models.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "-----------------------------------------------------------------------------------" );
        
        User u = new User(1,"Jean","Baton","jb@gmail.com");
        
        Gson gson = new Gson();
        String jsonstring = gson.toJson(u);
        
        System.out.println(jsonstring);
        String jsonString2 = "{\"id\":1,\"firstName\":\"Jean\",\"lastName\":\"Baton\",\"email\":\"jb@gmail.com\"}";
        User userObject = gson.fromJson(jsonString2, User.class);
        System.out.println(userObject);
        
        System.out.println("---------------------------------------------------");
        
        
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Justin");
        employee.setLastName("Bridou");
        employee.setRoles(Arrays.asList("ADMIN","MANAGER"));
        employee.setBirthDate(new Date("12/05/2000"));
        
//        Gson gson2 = new Gson();
//        String jsonEmploye = gson2.toJson(employee);
//        System.out.println(jsonEmploye);
//        String jsonStringEmployee = "{\"id\":1,\"firstName\":\"Justin\",\"lastName\":\"Bridou\",\"roles\":[\"ADMIN\",\"MANAGER\"]}";
//        
//        Employee employeeObject = gson.fromJson(jsonStringEmployee, Employee.class);
//        System.out.println(employeeObject);
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        Gson gson3 = gsonBuilder.create();
        
        //Convert to JSON
        System.out.println(gson3.toJson(employee));
        
       // Convert to java objects
        System.out.println(gson3.fromJson("{\"id\":1,\"firstName\":\"Justin\",\"lastName\":\"Bridou\",\"roles\":[\"ADMIN\",\"MANAGER\"],\"birthDate\":\"05/12/2000\"}", Employee.class));
        
    }
}
